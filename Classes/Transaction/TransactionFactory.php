<?php

namespace Classes\Transaction;

/**
 * Transaction Factory
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class TransactionFactory
{
    /**
     * Check what class we will use
     *
     * @param string $transactionClass [from config file]
     */
    public static function getClass(string $transactionClass) : ?TransactionInterface
    {
        // Check transaction list type
        switch ($transactionClass) {

            // Read transactions from text file
            case 'file':
                return new TransactionFromFileClass();
                // no break;
        }

        // If config is broken - return null
        return null;
    }
}