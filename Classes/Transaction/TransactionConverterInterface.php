<?php

namespace Classes\Transaction;

/**
 * Interface Converter
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
interface TransactionConverterInterface
{
    /**
     * Convert data to our format
     *
     * @param mixed $content
     * @return object|null
     *     @option string  bin      [Card BIN number, e.g. 123456]
     *     @option float   amount   [Amount of the transaction, e.g. 150000.00]
     *     @option string  currency [Currency of the operation, e.g. EUR]
     */
    public function convert($content) : ?object;
}