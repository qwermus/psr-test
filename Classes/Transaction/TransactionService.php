<?php

namespace Classes\Transaction;

use Exception;
use \Classes\BinInfo\BinInfoInterface;

/**
 * Transaction Service - class to work with transactions
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class TransactionService
{
    /**
     * Class to work with transactions
     */
    private $transactionClass;

    /**
     * Define Transaction class
     * @throws Exception
     */
    public function __construct()
    {
        /**
         * We need to read new transactions from the file (or other source in future)
         */
        if (is_null($this->transactionClass = (new TransactionFactory)->getClass(TRANSACTION_CLASS))) {
            throw new Exception('Wrong Transaction class ' . TRANSACTION_CLASS);
        }
    }

    /**
     * Get transactions list
     *
     * @param array $parameters
     * @return object
     * @throws Exception
     */
    public function getTransactionsList(array $parameters = []) : object
    {
        /**
         * Lets get object with transactions
         */
        if (is_null($transactionsList = $this->transactionClass->get($parameters))) {
            throw new Exception('There are no available transactions or input parameters are broken');
        }
        return $transactionsList;
    }
}