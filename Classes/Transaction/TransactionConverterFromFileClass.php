<?php

namespace Classes\Transaction;

/**
 * Class to convert data from file format to our inside format
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class TransactionConverterFromFileClass implements TransactionConverterInterface
{
    /**
     * @var array [converted data]
     */
    private $return = [];

    /**
     * Convert data from the file to common format
     *
     * @param string $content
     * @return object|null
     *     @option string  bin      [Card BIN number, e.g. 123456]
     *     @option float   amount   [Amount of the transaction, e.g. 150000.00]
     *     @option string  currency [Currency of the operation, e.g. EUR]
     */
    public function convert($content) : ?object
    {
        // Prepare content - explode it on different lines
        $content = $this->convertContent($content);

        // Iterate all lines and prepare line
        foreach ($content as $line) {

            // Check if line has all available keys
            // Otherwise we decide that file is broken and break our script
            if (is_null($checked = $this->convertLine($line))) {
                return null;
            }

            // Save line to a new array
            $this->return[] = $checked;
        }

        // Return new array as object
        return (object)$this->return;
    }

    /**
     * Prepare content to work with it lines in converter
     *
     * @param string $content
     * @return array
     */
    private function convertContent(string $content): array
    {
        // Remove empty spaces to destroy empty lines
        $content = trim($content);

        // Explode text on lines as array
        $exploded = preg_split("/((\r?\n)|(\r\n?))/", $content);

        // Return array
        return $exploded;
    }

    /**
     * Check if one line from the file is correct
     * Return line in our common format
     * While it based on text-file, keys of our format are same as in text file
     *
     * @param $line array
     * @return object|null
     */
    private function convertLine(string $line): ?object
    {
        // First of all we need to decode line
        if (!$line = json_decode($line)) {
            return null;
        }

        // If BIN is not correct
        if (!isset($line->bin) || !preg_match('/^[\d]{4,16}$/', $line->bin)) {
            return null;
        }

        // If Amount is not correct
        if (!isset($line->amount) || !preg_match('/^[\d]+\.[\d]{2}$/', $line->amount)) {
            return null;
        }

        // If Amount is not correct
        if (!isset($line->currency) || !preg_match('/^[A-Z]{3}$/', $line->currency)) {
            return null;
        }

        // Otherwise everything is ok
        return (object)[
            'bin' => $line->bin,
            'amount' => $line->amount,
            'currency' => $line->currency
        ];
    }
}