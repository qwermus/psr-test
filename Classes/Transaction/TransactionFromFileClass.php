<?php

namespace Classes\Transaction;

/**
 * Class to read transactions from the file
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class TransactionFromFileClass implements TransactionInterface
{
    /**
     * Class to convert data from the file to our common format
     *
     * @var string
     */
    private $converter;

    /**
     * Save file name to work with it later
     */
    public function __construct()
    {
        // Class to convert data from the file to our common format
        // We don't use fabric here because one converter is only for one transaction class
        $this->converter = new TransactionConverterFromFileClass();
    }

    /**
     * Get transactions list
     *
     * @param array $parameters
     * @return object|null
     */
    public function get(array $parameters = []) : ?object
    {
        // Read file and decode it to get an array
        if (is_null($content = $this->readfile($parameters['filename']))) {
            return null;
        }

        // Convert file content to our inside formatted object
        if (is_null($converted = $this->converter->convert($content))) {
            return null;
        }

        // Return object
        return $converted;
    }

    /**
     * Read file and return it's content
     *
     * @param string $filename
     * @return string|null
     */
    private function readfile(string $filename): ?string
    {
        // If there is no such file
        if (!is_file($filename)) {
            return null;
        }

        // Read file content
        if (!$content = file_get_contents($filename)) {
            return null;
        }

        return $content;
    }
}