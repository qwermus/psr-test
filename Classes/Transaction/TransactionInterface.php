<?php

namespace Classes\Transaction;

/**
 * Transaction Interface
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
interface TransactionInterface
{
    /**
     * Get transactions list
     *
     * @return object|null
     *     @option string  bin      [Card BIN number, e.g. 123456]
     *     @option float   amount   [Amount of the transaction, e.g. 150000.00]
     *     @option string  currency [Currency of the operation, e.g. EUR]
     */
    public function get() : ?object;
}