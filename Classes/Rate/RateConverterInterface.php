<?php

namespace Classes\Rate;

/**
 * Interface Converter
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
interface RateConverterInterface
{
    /**
     * Convert data to our format
     *
     * @param mixed $content
     * @return object|null
     */
    public function convert($content) : ?object;
}