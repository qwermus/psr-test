<?php

namespace Classes\Rate;

/**
 * Rate From ExchangeRatesApi
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class RateFromExchangeRatesApiClass implements RateInterface
{
    /**
     * URI to get rates
     * It can be moved to config file
     *
     * @var string
     */
    private $uri = EXCHANGERATESAPI_URI;


    /**
     * Class to convert data from the exchangeratesapi to our common format
     *
     * @var string
     */
    private $converter;

    /**
     * Constructor - define converter class
     *
     * @param array $parameters
     */
    public function __construct()
    {
        // Class to convert data from the exchangeratesapi to our common format
        // We don't use fabric here because one converter is only for one class
        $this->converter = new RateConverterFromExchangeRatesApiClass();
    }

    /**
     * Get rate from the exchangeratesapi
     *
     * @param array $european [List of european countries]
     * @return object|null
     */
    public function get() : ?object
    {
        // Read file and decode it to get an array
        if (is_null($content = $this->readuri())) {
            return null;
        }

        // Convert file content to our inside formatted object
        if (is_null($converted = $this->converter->convert($content))) {
            return null;
        }

        // Return object
        return $converted;
    }

    /**
     * Read rates file
     *
     * @return false|string
     */
    private function readuri() : ?string
    {
        if (is_null($content = curlHelper($this->uri))) {
            return null;
        }
        return $content;
    }
}
