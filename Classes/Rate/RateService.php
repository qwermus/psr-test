<?php

namespace Classes\Rate;

use Exception;

/**
 * Rate Service - class to work with rates
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class RateService
{
    /**
     * Class to work with rates
     */
    private $rateClass;

    /**
     * Define Rates class
     * @throws Exception
     */
    public function __construct()
    {
        /**
         * NHere we define Rate Class getter
         */
        if (is_null($this->rateClass = (new RateFactory)->getClass(RATE_CLASS))) {
            throw new Exception('Wrong Rate class ' . RATE_CLASS);
        }
    }

    /**
     * Get rates list
     *
     * @return object
     * @throws Exception
     */
    public function getRatesList() : object
    {
        /**
         * Try to get rates list
         */
        if (is_null($rates = $this->rateClass->get())) {
            throw new Exception('There are no available rates');
        }
        return $rates;
    }
}