<?php

namespace Classes\Rate;

/**
 * Rate Factory
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class RateFactory
{
    /**
     * Check what class we will use
     *
     * @param $rateClass [from config file]
     */
    public static function getClass(string $rateClass) : ?RateInterface
    {
        // Check rate list type
        switch ($rateClass) {

            // Read rates from 'https://api.exchangeratesapi.io/latest'
            case 'exchangeratesapi':
                return new RateFromExchangeRatesApiClass();
                // no break;
        }

        // If config is broken - return null
        return null;
    }
}