<?php

namespace Classes\Rate;

/**
 * Interface RateInterface
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
interface RateInterface
{
    /**
     * Get rate
     *
     * @return object|null
     */
    public function get() : ?object;
}