<?php

namespace Classes\Rate;

/**
 * Class to convert data from ExchangeRatesApi format to our inside format
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class RateConverterFromExchangeRatesApiClass implements RateConverterInterface
{
    /**
     * Comissions list
     * By default all european countries are index 'one'
     * @var array [converted data]
     */
    private $return = [];

    /**
     * Convert data from the ExchangeRatesApi to common format
     *
     * @param string $content
     * @return object|null
     */
    public function convert($content) : ?object
    {
        // Prepare content - just decode it
        if (!$content = json_decode($content)) {
            return null;
        }

        // And check if there are rates, otherwise something is broken
        if (!isset($content->rates)) {
            return null;
        }

        // Save base currency
        if (isset($content->base) && is_string($content->base)) {
            $this->saveCurrency($content->base, 1);
        }

        // Iterate all currencies and save them
        foreach ($content->rates as $currency => $rate) {
            $this->saveCurrency($currency, $rate);
        }

        // Return new array as object
        return (object)$this->return;
    }

    /*
     * Save one currency to result array
     */
    private function saveCurrency(string $currency, float $rate)
    {
        $this->return[$currency] = $rate;
    }
}