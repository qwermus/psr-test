<?php

namespace Classes\Comission;

/**
 * Class to calculate a comission
 * @package Classes\Comission
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class ComissionClass implements ComissionInterface
{
    /**
     * All EU countries, from config file
     * @var string[]
     */
    private $EuCountries = EUROPEAN_COUNTRIES;

    /**
     * Comission for EU countries
     * @var float
     */
    private $euComissionIndex = EUROPEAN_COUNTRIES_COMISSION_INDEX;

    /**
     * Comission for non-EU countries
     * @var float
     */
    private $nonEuComissionIndex = NON_EUROPEAN_COUNTRIES_COMISSION_INDEX;

    /**
     * Calculate comission
     *
     * @param float $amount
     * @param float|null $rate
     * @param string $country
     * @return float|null
     */
    public function getComission(float $amount, ?float $rate, string $country = '') : ?float
    {
        // Something wrong with rate
        if (is_null($rate) || $rate <= 0) {
            return null;
        }

        // Get comission index
        $index = $this->comissionIndex($country);

        // Return comission
        return ( $amount / $rate ) * $index;
    }

    /**
     * Create list of EU countries with static rate
     *
     * @param string $country
     * @return float
     */
    private function comissionIndex(string $country) : float
    {
        if (in_array($country, $this->EuCountries)) {
            return $this->euComissionIndex;
        }

        return $this->nonEuComissionIndex;
    }

    /**
     * Get EU comission Index to use in unit tests
     * @return float
     */
    public function getEuComissionIndex()
    {
        return $this->euComissionIndex;
    }

    /**
     * Get NON-EU comission Index to use in unit tests
     * @return float
     */
    public function getNonEuComissionIndex()
    {
        return $this->nonEuComissionIndex;
    }
}