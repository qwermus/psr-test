<?php

namespace Classes\Comission;

/**
 * Interface Comission
 * @package Classes\Comission
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
interface ComissionInterface
{
    /**
     * Get comission having only amount and rate
     * @param float $amount
     * @param float $rate
     * @return float|null
     */
    public function getComission(float $amount, float $rate) : ?float;
}