<?php

namespace Classes\Comission;

/**
 * Comission Factory
 * @package Classes\Rate
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class ComissionFactory
{
    /**
     * Alghorytm to calculate a comission
     *
     * @param $comissionClass [from config file]
     */
    public static function getClass(string $comissionClass) : ?ComissionInterface
    {
        // Check comission alghorytm type
        switch ($comissionClass) {

            // Our first alghorytm
            case 'first':
                return new ComissionClass();
                // no break;
        }

        // If config is broken - return null
        return null;
    }
}