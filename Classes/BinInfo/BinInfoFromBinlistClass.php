<?php

namespace Classes\BinInfo;

/**
 * Class to read BIN info from the binlist
 * @package Classes\Transaction
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class BinInfoFromBinlistClass implements BinInfoInterface
{
    /**
     * URI to get BIN information
     *
     * @var string
     */
    private $uri = BINLIST_URI;

    /**
     * Get country by BIN number
     *
     * @param string $bin
     * @return object|null
     */
    public function country(string $bin) : ?string
    {
        // Read file and decode it to get an array
        if (is_null($content = $this->readinfo($bin))) {
            return null;
        }

        // Return object
        return $this->getAlpha2($content);
    }

    /**
     * Read BIN info and return it's content
     *
     * @return array|null
     */
    private function readinfo(string $bin): ?object
    {
        $uri = $this->createUri($bin);
        if (is_null($content = curlHelper($uri))) {
            return null;
        }

        // Try to decode content
        if (!$content = json_decode($content)) {
            return null;
        }

        return $content;
    }

    /**
     * Create uri for request
     *
     * @param string $bin
     * @return string
     */
    private function createUri(string $bin) : string
    {
        return str_replace('{BIN}', $bin, $this->uri);
    }

    /**
     * Get country alpha2 from object
     *
     * @param string $bin
     * @return object|null
     */
    private function getAlpha2(object $content) : ?string
    {
        // Check if country isset
        if (!isset($content->country) || !isset($content->country->alpha2)) {
            return null;
        }

        // Return country
        return $content->country->alpha2;
    }
}