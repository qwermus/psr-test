<?php

namespace Classes\BinInfo;

/**
 * BinInfo Factory
 * @package Classes\BinInfo
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class BinInfoFactory
{
    /**
     * Check what class we will use
     *
     * @param string $binInfoClass [from config file]
     */
    public static function getClass(string $binInfoClass) : ?BinInfoInterface
    {
        // Check BIN info config parameter
        switch ($binInfoClass) {

            // Read BIN info from text Binlist service
            case 'binlist':
                return new BinInfoFromBinlistClass();
                // no break;
        }

        // If config is broken - return null
        return null;
    }
}