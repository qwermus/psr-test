<?php

namespace Classes\BinInfo;

/**
 * BinInfo Interface
 * @package Classes\BinInfo
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
interface BinInfoInterface
{
    /**
     * Get Country by BIN number
     *
     * @param string $bin
     * @return object|null
     */
    public function country(string $bin) : ?string;
}