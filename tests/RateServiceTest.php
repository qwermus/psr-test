<?php

declare(strict_types=1);

use Classes\Rate\RateService;
use PHPUnit\Framework\TestCase;

/**
 * Test rate service
 */
final class RateServiceTest extends TestCase
{
    /**
     * test Rate Service
     */
    public function testRateService(): void
    {
        $rates = (new RateService())->getRatesList();
        $this->assertIsObject($rates);
        $this->assertObjectHasAttribute('EUR', $rates);
        $this->assertObjectHasAttribute('USD', $rates);
    }
}