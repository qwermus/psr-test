<?php

declare(strict_types=1);

use Classes\Transaction\TransactionFromFileClass;
use PHPUnit\Framework\TestCase;

/**
 * Test transaction classes
 */
final class TransactionTest extends TestCase
{
    /**
     * @var string [test transactions]
     */
    private $filename = TESTING_INPUT;

    /**
     * test TransactionFromFile Class
     */
    public function testTransactionFromFileClass(): void
    {
        $transactions = (new TransactionFromFileClass())->get(['filename' => $this->filename]);
        $this->assertIsObject($transactions);
        $this->assertObjectHasAttribute('bin', $transactions->{0});
        $this->assertObjectHasAttribute('amount', $transactions->{0});
        $this->assertObjectHasAttribute('currency', $transactions->{0});
    }
}