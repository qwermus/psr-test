<?php

declare(strict_types=1);

use Classes\Rate\RateFromExchangeRatesApiClass;
use PHPUnit\Framework\TestCase;

/**
 * Test rates classes
 */
final class RateTest extends TestCase
{
    /**
     * test RateFromExchangeRatesApi Class
     */
    public function testRateFromExchangeRatesApiClass(): void
    {
        $rates = (new RateFromExchangeRatesApiClass())->get();
        $this->assertIsObject($rates);
        $this->assertObjectHasAttribute('EUR', $rates);
        $this->assertObjectHasAttribute('USD', $rates);
    }
}