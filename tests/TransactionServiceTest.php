<?php

declare(strict_types=1);

use Classes\Transaction\TransactionService;
use PHPUnit\Framework\TestCase;

/**
 * Test transaction Service
 */
final class TransactionServiceTest extends TestCase
{
    /**
     * @var string [test transactions]
     */
    private $filename = TESTING_INPUT;

    /**
     * test Transaction Service
     */
    public function testTransactionService(): void
    {
        $transactions = (new TransactionService())->getTransactionsList(['filename' => $this->filename]);
        $this->assertIsObject($transactions);
        $this->assertObjectHasAttribute('bin', $transactions->{0});
        $this->assertObjectHasAttribute('amount', $transactions->{0});
        $this->assertObjectHasAttribute('currency', $transactions->{0});
    }
}