<?php

declare(strict_types=1);

use Classes\Comission\ComissionClass;
use PHPUnit\Framework\TestCase;

/**
 * Test comissions calculating
 */
final class ComissionTest extends TestCase
{
    /**
     * test Comission Class
     */
    public function testComissionClass(): void
    {
        $comissionClass = new ComissionClass();

        $this->assertEquals(
            ( 1000 / 0.25 ) * $comissionClass->getEuComissionIndex(),
            $comissionClass->getComission(1000, 0.25, 'DK')
        );

        $this->assertEquals(
            ( 1000 / 0.25 ) * $comissionClass->getNonEuComissionIndex(),
            $comissionClass->getComission(1000, 0.25, 'JP')
        );

        $this->assertEquals(null, $comissionClass->getComission(1000, null, 'JP'));
        $this->assertEquals(null, $comissionClass->getComission(1000, 0, 'JP'));
        $this->assertEquals(null, $comissionClass->getComission(1000, -1, 'JP'));


    }
}