<?php

declare(strict_types=1);

use Classes\BinInfo\BinInfoFromBinlistClass;
use PHPUnit\Framework\TestCase;

/**
 * Test country by BIN classes
 */
final class BinInfoTest extends TestCase
{
    /**
     * test BinInfoFromBinlist Class
     */
    public function testBinInfoFromBinlistClass(): void
    {
        $this->assertEquals('DK', (new BinInfoFromBinlistClass())->country('45717360'));
        $this->assertEquals('LT', (new BinInfoFromBinlistClass())->country('516793'));
        $this->assertEquals('JP', (new BinInfoFromBinlistClass())->country('45417360'));
    }
}