<?php


/**
 * We do not always need all the decimal places
 * Sometimes we need ceiling
 *
 * @param float $number
 * @return float
 */
if (!function_exists('ceilHelper')) {
    function ceilHelper(float $number): float
    {
        return ceil($number * 100) / 100;
    }
}