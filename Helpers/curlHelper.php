<?php

/**
 * Get remote information
*/
if (!function_exists('curlHelper')) {
    function curlHelper($uri)
    {
        // Try to init curl
        if (!$ch = curl_init($uri)) {
            return null;
        }

        // Exec curl
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);

        // If there is no info - something is wrong
        if (curl_errno($ch)) {
            return null;
        }

        // Check code. If it is not 200 - something is wrong
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
            return null;
        }

        // Close curl
        curl_close($ch);

        // Otherwise return a content
        return $content;
    }
}