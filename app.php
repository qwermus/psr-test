<?php

/**
 * First of all let's require autoload
 */
ini_set('display_errors', true);
require_once('vendor/autoload.php');

/**
 * What classes will we use in this script
 */
use \Classes\Rate\RateService;
use \Classes\BinInfo\BinInfoFactory;
use \Classes\Comission\ComissionFactory;
use \Classes\Transaction\TransactionService;

/**
 * Let's get rates
 */
$rates = (new RateService())->getRatesList();

/**
 * Next step is to define BIN info Class...
 */
if (is_null($binInfoClass = (new BinInfoFactory)->getClass(BININFO_CLASS))) {
    throw new Exception('Wrong BIN info class ' . BININFO_CLASS);
}

/**
 * ...and comission Class
 */
if (is_null($comissionClass = (new ComissionFactory)->getClass(COMISSION_CLASS))) {
    throw new Exception('Wrong comission class ' . COMISSION_CLASS);
}

/**
 * We need to read new transactions from the file (or other source in future)
 */
$transactions = (new TransactionService())->getTransactionsList(['filename' => $argv[1]]);

/**
 * And calculateble part of script - get commissions and print 'em
 */
$result = [];
foreach ($transactions as $transaction) {

    // Get country of BIN number
    if (is_null($country = $binInfoClass->country($transaction->bin))) {
        throw new Exception('Can not find country by BIN ' . $transaction->bin);
    }

    // Calculate comission
    if (is_null($comission = $comissionClass->getComission($transaction->amount, $rates->{$transaction->currency} ?? null, $country))) {
        throw new Exception('Can not calculate comission');
    }

    // Ceil and save comission to result array
    $result[] = ceilHelper($comission);
}

die(implode("\n", $result)."\n");