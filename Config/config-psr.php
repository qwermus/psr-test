<?php

/**
 * Transactions source.
 * Now available only "file"
 */
define('TRANSACTION_CLASS', 'file');

/**
 * Class to calculate rates
 * Now available only "exchangeratesapi"
 */
define('RATE_CLASS', 'exchangeratesapi');

/**
 * Class to get BIN info
 * Now available only "binlist"
 */
define('BININFO_CLASS', 'binlist');

/**
 * Define alghorytm to calculate comission
 * Now available only "first"
 */
define('COMISSION_CLASS', 'first');

/**
 * URI for exchangeratesapi
 */
define('EXCHANGERATESAPI_URI', 'https://api.exchangeratesapi.io/latest');

/**
 * URI for binlist
 */
define('BINLIST_URI', 'https://lookup.binlist.net/{BIN}');

/**
 * List all european countries. Their rate is "one"
 */
define('EUROPEAN_COUNTRIES', [
        'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU',
        'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK',
    ]
);

/**
 * Define comissions for EU and NON-EU countries
 */
define('EUROPEAN_COUNTRIES_COMISSION_INDEX', 0.01);
define('NON_EUROPEAN_COUNTRIES_COMISSION_INDEX', 0.02);

/**
 * Transactions file for testing
 */
define('TESTING_INPUT', 'input.txt');